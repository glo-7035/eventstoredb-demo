
import java.time.LocalDateTime;

public class Record {
    public int Id;
    public String Name;
    public LocalDateTime LastUpdate;

    @Override
    public String toString() {
        return "----- Record -----\n" +
                "Id: " + Id + "\n" +
                "Name: " + Name + "\n" +
                "LastUpdate: " + LastUpdate + "\n";
    }
}
