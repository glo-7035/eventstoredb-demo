import akka.actor.*;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import events.EventType;
import events.IRecordRepository;
import events.RecordEvent;
import eventstore.akka.Settings;
import eventstore.akka.tcp.ConnectionActor;
import eventstore.core.*;
import eventstore.j.EventDataBuilder;
import eventstore.j.ReadEventBuilder;
import eventstore.j.SettingsBuilder;
import eventstore.j.WriteEventsBuilder;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class Program {

    /**
     * nom du stream d'event qui contient tous les événements reliés aux Records
     * */
    private static final String RECORD_STREAM = "record-stream";

    private AtomicBoolean writePerformed = new AtomicBoolean(false);

    public static void main(String[] args) {
        Program prog = new Program();
        try {
            prog.run();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

    public void run() throws IOException, ExecutionException, InterruptedException {

        /**
         * Aggregate des objets du domaine "Record" en mémoire vive
         * */
        RecordAggregate recordAggregate = new RecordAggregate(initialRecords());

        // 0. Initilization de la BD
        ActorSystem system = ActorSystem.create();

        Settings settings = new SettingsBuilder()
                .address(new InetSocketAddress("127.0.0.1", 1113))
                .defaultCredentials("admin", "changeit")
                .build();
        ActorRef connection = system.actorOf(ConnectionActor.getProps(settings));
        ActorRef readResult = system.actorOf(ReadResult.props(recordAggregate));
        ActorRef writeResult = system.actorOf(WriteResult.props(writePerformed));

        //État initiales
        recordAggregate.print();

        // 1. Création d'un Record
        publishCreateEventExample(connection, readResult, writeResult);
        readFromEventStream(connection, readResult);

        // 2. Modification d'un Record
        /*publishUpdateEventExample(connection, readResult, writeResult);
        readFromEventStream(connection, readResult);*/

        // 2. Destruction d'un Record
        /*publishDeleteEventExample(connection, readResult, writeResult);
        readFromEventStream(connection, readResult);*/

    }

    private void publishCreateEventExample(ActorRef connection, ActorRef readResult, ActorRef writeResult) throws IOException, ExecutionException, InterruptedException {
        RecordEvent recordEvent = new RecordEvent();
        recordEvent.Type = EventType.Create;
        recordEvent.RecordId = 4;
        recordEvent.RecordName = "Jonhnny Cash";
        EventData event = generateEvent(recordEvent);
        publishToEventStream(connection, writeResult, event);
        waitForWriteComplete();
    }

    private void publishUpdateEventExample(ActorRef connection, ActorRef readResult, ActorRef writeResult) throws IOException, ExecutionException, InterruptedException {
        RecordEvent recordEvent = new RecordEvent();
        recordEvent.Type = EventType.Update;
        recordEvent.RecordId = 1;
        recordEvent.RecordName = "ALAIN";
        EventData event = generateEvent(recordEvent);
        publishToEventStream(connection, writeResult, event);
        waitForWriteComplete();
    }

    private void publishDeleteEventExample(ActorRef connection, ActorRef readResult, ActorRef writeResult) throws IOException, ExecutionException, InterruptedException {
        RecordEvent recordEvent = new RecordEvent();
        recordEvent.Type = EventType.Delete;
        recordEvent.RecordId = 1;
        EventData event = generateEvent(recordEvent);
        publishToEventStream(connection, writeResult, event);
        waitForWriteComplete();
    }

    private void readFromEventStream(ActorRef connection, ActorRef readResult) throws IOException, ExecutionException, InterruptedException {
        ReadEvent readEvent = new ReadEventBuilder(RECORD_STREAM)
                .last()
                .resolveLinkTos(false)
                .requireMaster(true)
                .build();
        connection.tell(readEvent, readResult);
    }

    private void publishToEventStream(ActorRef connection, ActorRef writeResult, EventData event) {
        WriteEvents writeEvents = new WriteEventsBuilder(RECORD_STREAM)
                .addEvent(event)
                .expectAnyVersion()
                .build();
        connection.tell(writeEvents, writeResult);
    }

    private void waitForWriteComplete() throws InterruptedException {
        while (!writePerformed.get()) {
            Thread.sleep(100);
        }
    }

    private static EventData generateEvent(RecordEvent recordEvent) throws IOException {
        return new EventDataBuilder(recordEvent.Type.toString())
                .eventId(UUID.randomUUID())
                .jsonData(recordEvent.toJsonString())
                .jsonMetadata(recordEvent.toString() + "_Record_" + recordEvent.RecordId)
                .build();
    }

    private static List<Record> initialRecords() {
        Record aRecord = new Record();
        aRecord.Id = 1;
        aRecord.Name = "Alice";
        aRecord.LastUpdate = LocalDateTime.now();

        Record anotherRecord = new Record();
        anotherRecord.Id = 2;
        anotherRecord.Name = "Bob";
        anotherRecord.LastUpdate = LocalDateTime.now();

        Record aThirdRecord = new Record();
        aThirdRecord.Id = 3;
        aThirdRecord.Name = "Martin";
        aThirdRecord.LastUpdate = LocalDateTime.now();

        List output = Collections.synchronizedList(new ArrayList<Record>());
        output.add(aRecord);
        output.add(anotherRecord);
        output.add(aThirdRecord);
        return output;
    }

    private static class RecordAggregate implements IRecordRepository {
        private List<Record> records;

        public RecordAggregate(List<Record> alreadyThereRecords) {
            records = alreadyThereRecords;
        }

        public void Add(RecordEvent recordEvent) {
            Record newRecord = new Record();
            newRecord.Id = recordEvent.RecordId;
            newRecord.Name = recordEvent.RecordName;
            newRecord.LastUpdate = LocalDateTime.now();
            records.add(newRecord);
        }

        public void Update(RecordEvent recordEvent) {
            records.stream()
                    .filter(r -> r.Id == recordEvent.RecordId)
                    .findFirst()
                    .ifPresent(r -> {
                        r.Id = recordEvent.RecordId;
                        r.Name = recordEvent.RecordName;
                        r.LastUpdate = LocalDateTime.now();
                    });
        }

        public void Delete(RecordEvent recordEvent) {
            Optional<Record> record = records.stream()
                    .filter(r -> r.Id == recordEvent.RecordId)
                    .findFirst();
            record.ifPresent(value -> records.remove(value));
        }

        public void print() {
            for (Record record : records) {
                System.out.println(record);
            }
        }
    }

    public static class ReadResult extends AbstractActor {
        private final LoggingAdapter log = Logging.getLogger(getContext().system(), this);
        private final IRecordRepository recordRepository;

        static Props props(IRecordRepository recordRepository) {
            return Props.create(ReadResult.class, () -> new ReadResult(recordRepository));
        }

        public ReadResult(IRecordRepository observer) {
            this.recordRepository = observer;
        }

        @Override
        public Receive createReceive() {
            return receiveBuilder()
                    .match(ReadEventCompleted.class, m -> {
                        final Event event = m.event();
                        RecordEvent recordEvent = RecordEvent.fromByteString(event.data().data().value());
                        OnEventReceived(recordEvent);
                        context().system().terminate();
                    })
                    .match(Status.Failure.class, f -> {
                        final EsException exception = (EsException) f.cause();
                        log.error(exception, exception.toString());
                        context().system().terminate();
                    })
                    .build();
        }

        private void OnEventReceived(RecordEvent event) {
            switch (event.Type) {
                case Create:
                    recordRepository.Add(event);
                    break;
                case Update:
                    recordRepository.Update(event);
                    break;
                case Delete:
                    recordRepository.Delete(event);
                    break;
            }

            recordRepository.print();
        }
    }

    public static class WriteResult extends AbstractActor {

        private AtomicBoolean writePerformed;

        static Props props(AtomicBoolean writePerformed) {
            return Props.create(WriteResult.class, () -> new WriteResult(writePerformed));
        }

        public WriteResult(AtomicBoolean writePerformed) {
            this.writePerformed = writePerformed;
        }

        @Override
        public Receive createReceive() {
            return receiveBuilder()
                    .match(WriteEventsCompleted.class, m -> {
                        System.out.println("APPENDING RECORD EVENT TO STREAM");
                        System.out.println();
                        writePerformed.compareAndSet(false, true);
                        context().system().terminate();
                    })
                    .match(Status.Failure.class, f -> {
                        final EsException exception = (EsException) f.cause();
                        System.out.println("ERROR WHILE WRITING TO STREAM : " + exception.getMessage());
                        System.out.println();
                    })
                    .build();
        }

    }

}
