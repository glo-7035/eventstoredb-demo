package events;

import com.fasterxml.jackson.databind.ObjectMapper;
import eventstore.core.ByteString;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class RecordEvent {
    public EventType Type;
    public int RecordId;
    public String RecordName;

    public String toJsonString() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(this);
    }

    public static RecordEvent fromByteString(ByteString byteString) throws IOException {
        String stringValue = new String(byteString.toArray(), StandardCharsets.UTF_8);
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(stringValue, RecordEvent.class);
    }
}
