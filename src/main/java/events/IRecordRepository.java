package events;

public interface IRecordRepository {
    void Add(RecordEvent event);
    void Update(RecordEvent event);
    void Delete(RecordEvent event);
    void print();
}
