package events;

public enum EventType {
    Create,
    Update,
    Delete;

    public static EventType from(String eventType) {
        for (EventType type : EventType.values()) {
            if (type.toString().equals(eventType)) {
                return type;
            }
        }

        throw new RuntimeException("provided even type is wrong");
    }
}
